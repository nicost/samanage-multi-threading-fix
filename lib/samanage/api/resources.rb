# frozen_string_literal: true

module Samanage
  class Api
    RESOURCES = %i[
      attachment
      category
      change
      contract
      configuration_item
      custom_field
      custom_fields
      custom_form
      custom_forms
      department
      group
      hardware
      incident
      mobile
      other_asset
      problem
      purchase_order
      release
      site
      task
      solution
      user
      vendor
    ]
  end
end
